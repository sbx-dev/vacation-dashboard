import React from 'react';
import {Container} from "react-bootstrap";
import Header from "./header";

const Layout = ({children, profile}) => (
  <>
    <Header profile={profile}/>
    <Container fluid>
      {children}
    </Container>
  </>
);

export default Layout;