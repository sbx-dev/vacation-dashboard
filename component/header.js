import React from "react";
import Link from "next/link";
import { Navbar } from "react-bootstrap";

const Header = ({ profile }) => (
  <Navbar>
    <Navbar.Brand>
      {/* <Link href={`/`}>
        <a>SocialBox Admin</a>
      </Link> */}
      소셜박스 휴가 조회
    </Navbar.Brand>
    <Navbar.Collapse className={`justify-content-end`}>
      <Navbar.Text style={{ marginRight: `0.2rem` }}>
        {profile.displayName}
      </Navbar.Text>
      <Link href={`/oauth/logout`}>
        <a>Logout</a>
      </Link>
    </Navbar.Collapse>
  </Navbar>
);

export default Header;
