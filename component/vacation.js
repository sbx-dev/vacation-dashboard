import React, {useEffect, useState} from 'react';
import {Card, Col} from "react-bootstrap";
import axios from 'axios';

const Vacation = () => {
  const [timeMin, setTimeMin] = useState('');
  const [timeMax, setTimeMax] = useState('');
  const [data, setData] = useState();

  const getData = async (callback) => {
    const {data} = await axios.get('api/vacation', {
      params : {
        timeMin,
        timeMax
      }
    });

    callback && callback(data);
    return data;
  };

  const createTable = () => {

  };

  useEffect(() => {
    getData(setData);
  }, []);

  return (
    <Col>
      <Card>
        <Card.Header className="text-center">Vacation Table</Card.Header>
        <Card.Body>
          {createTable()}
        </Card.Body>
      </Card>
    </Col>
  )
};

export default Vacation;