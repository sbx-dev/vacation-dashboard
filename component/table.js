import React from 'react';
import {Card, Col, Row, Spinner} from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

const Table = ({title, keyField, columns, data = [], defaultSorted, pagenation}) => (
  <Row>
    <Col>
      <Card>
        <Card.Header className={`text-center`}>{title}</Card.Header>
        <Card.Body>
          <BootstrapTable
            classes="text-center"
            condensed
            striped
            hover
            bootstrap4
            keyField={keyField}
            columns={columns}
            data={data}
            noDataIndication={() => <Spinner animation="border"/>}
            defaultSorted={ defaultSorted }
            pagination={ pagenation ? paginationFactory() : undefined }
          />
        </Card.Body>
      </Card>
    </Col>
  </Row>
);

export default Table;