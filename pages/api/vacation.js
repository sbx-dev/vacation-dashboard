import axios from "axios";
import moment from "moment";
import database from "../../lib/database";

const saveHolidays = async (download) => {
  if (!download.length) {
    return undefined;
  }

  try {
    await database.query(
      `INSERT INTO restdays (name, date, year, month, day, dow, type, holiday, seq) VALUES ?`,
      [download]
    );
  } catch (e) {
    await saveHolidays(download);
  }
};
const downloadHolidays = async (marker) => {
  try {
    const {
      data: {
        response: {
          body: {
            items: {
              item = {
                dateKind: "00",
                dateName: "marker",
                isHoliday: "N",
                seq: 0,
                flag: true,
              },
            },
          },
        },
      },
    } = await axios({
      url:
        "http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService/getRestDeInfo",
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36",
        "Content-Type": "application/json",
      },
      params: {
        serviceKey: process.env.HOLIDAY_API_KEY,
        solYear: marker.format("YYYY"),
        solMonth: marker.format("MM"),
        numOfRows: 30,
      },
    });

    return (Array.isArray(item) ? item : [item]).map((row) => {
      const { dateName, locdate, dateKind, isHoliday, seq, flag } = row;
      const date = !flag ? moment.utc(`${locdate}`) : marker;

      return [
        dateName,
        date.format(!flag ? "YYYYMMDD" : "YYYYMM00"),
        date.year(),
        date.month() + 1,
        !flag ? date.date() : 0,
        !flag ? date.day() : -1,
        dateKind,
        isHoliday,
        seq,
      ];
    });
  } catch (e) {
    return await downloadHolidays(marker);
    // throw e;
  }
};
const loadHolidays = async (marker) => {
  try {
    const [
      rows,
    ] = await database.query(
      `SELECT * FROM restdays WHERE year = ? AND month = ?`,
      [marker.year(), marker.month() + 1]
    );
    return rows;
  } catch (e) {
    throw e;
  }
};

const getHolidaysPromise = async (marker) => {
  let rows = [];

  rows = await loadHolidays(marker);
  if (!rows.length) {
    let download = await downloadHolidays(marker);
    await saveHolidays(download);
    rows = await loadHolidays(marker);
  }

  return rows;
};
const getHolidays = async (startDate, endDate) => {
  const count = Math.ceil(endDate.diff(startDate, "months", true));
  const promiseArray = [...Array(count)].map((value, index) => {
    const marker = moment(startDate).add(index, "months");
    return getHolidaysPromise(marker);
  });
  const promiseResult = await Promise.all(promiseArray);
  const promiseResultArr = promiseResult.reduce(
    (resultArray, curruntArray) => [...resultArray, ...curruntArray],
    []
  );
  return promiseResultArr.map((value) => value.date);
};

const downloadEvents = async (startDate, endDate, accessToken, pageToken) => {
  try {
    const { data } = await axios({
      url: `https://www.googleapis.com/calendar/v3/calendars/${process.env.GOOGLE_CALENDER_ID}/events`,
      headers: { Authorization: `Bearer ${accessToken}` },
      params: {
        timeMin: startDate.toDate(),
        timeMax: endDate.toDate(),
        maxResults: 2500,
        singleEvents: true,
        orderBy: "startTime",
        pageToken,
      },
    });
    return data;
  } catch (e) {
    return await downloadEvents(startDate, endDate, accessToken, pageToken);
  }
};
const getEventsRecursive = async (
  startDate,
  endDate,
  accessToken,
  pageToken
) => {
  try {
    const { items, nextPageToken } = await downloadEvents(
      startDate,
      endDate,
      accessToken,
      pageToken
    );
    return nextPageToken
      ? items.concat(
          await getEventsRecursive(
            startDate,
            endDate,
            accessToken,
            nextPageToken
          )
        )
      : items;
  } catch (e) {
    throw e;
  }
};
const getEvents = async (startDate, endDate, accessToken) => {
  try {
    const items = await getEventsRecursive(startDate, endDate, accessToken);
    return items.reduce((result, current) => {
      const {
        summary,
        end: { date: endDate, dateTime: endDateTime },
        start: { date: startDate, dateTime: startDateTime },
      } = current;

      const [name, type] = summary.split(" ");
      const eventStart = (startDate
        ? moment.utc(startDate)
        : moment.utc(startDateTime)
      ).startOf("day");
      const eventEnd = (endDate
        ? moment.utc(endDate).subtract(1, "days")
        : moment.utc(endDateTime)
      ).endOf("day");

      return [...result, { name, type, eventStart, eventEnd }];
    }, []);
  } catch (e) {
    throw e;
  }
};

const daysArr = (startDate, endDate, holidays) => {
  const count = Math.ceil(endDate.diff(startDate, "days", true));
  return [...Array(count)].reduce((result, current, index) => {
    const marker = moment(startDate).add(index, "days");
    const date = marker.format("YYYYMMDD");
    const day = marker.day();
    return day === 0 || day === 6 || holidays.includes(date)
      ? result
      : [...result, date];
  }, []);
};
const monthsArr = (startDate, endDate) => {
  const count = Math.ceil(endDate.diff(startDate, "months", true));
  return [...Array(count)].reduce((result, current, index) => {
    const marker = moment(startDate).add(index, "months");
    const month = marker.format("YYYYMM");
    return [...result, month];
  }, []);
};
const yearsArr = (startDate, endDate) => {
  const count = Math.ceil(endDate.diff(startDate, "years", true));
  return [...Array(count)].reduce((result, current, index) => {
    const marker = moment(startDate).add(index, "years");
    const year = marker.format("YYYY");
    return [...result, year];
  }, []);
};

/**
 * 소셜박스 근무자들 2020-12-02까지
 */
const worker = [
  "김지은",
  "니콜라",
  "전희나",
  "서호범",
  "양태식",
  "서영민",
  "김의섭",
  "강지연",
  "황성준",
  "남수진",
  "오형민",
  "김슬기",
  "김도현",
  "송형우",
  "이진수",
];

/**
 * 퇴사자를 제외한 근무자들 이름 목록 반환
 * @param {Array} events 이벤트 목록
 */
const namesArr = (events) => {
  const namesArr = events.reduce((result, current) => {
    const { name } = current;
    // result 배열 안에 name이 포함되어 있으면 더하지 않고 반환, 아니면 더함
    return result.includes(name) ? result : [...result, name];
  }, []);
  // 위에서 생성된 배열에서 worker 배열에 포함된 값만 들어간 배열을 반환
  return namesArr.filter((name) => worker.includes(name));
};

const getCount = (type) =>
  /공가|병가|대체/.test(type)
    ? 0
    : /반차|오전|오후/.test(type)
    ? 0.5
    : /휴가|연차/.test(type)
    ? 1
    : 0;

export default async (req, res) => {
  const {
    session: {
      passport: {
        user: { accessToken },
      },
    },
    query: { timeMin, timeMax, userId },
  } = req;

  const startDate = (timeMin ? moment.utc(timeMin) : moment.utc()).startOf(
    "year"
  );

  const endDate = (timeMax ? moment.utc(timeMax) : moment.utc()).endOf("year");

  try {
    const holidays = await getHolidays(startDate, endDate);
    const events = await getEvents(startDate, endDate, accessToken);
    const days = daysArr(startDate, endDate, holidays);
    const months = monthsArr(startDate, endDate);
    const years = yearsArr(startDate, endDate);
    const names = namesArr(events);

    const eventDaysMap = days.reduce((dayResult, dayCurrent) => {
      const eventFilter = events.filter(({ eventStart, eventEnd }) =>
        moment.utc(dayCurrent).isBetween(eventStart, eventEnd, "day", "[]")
      );
      return eventFilter.length
        ? { ...dayResult, [dayCurrent]: eventFilter }
        : dayResult;
    }, {});
    const daysMap = Object.entries(eventDaysMap).reduce(
      (result, [key, eventList]) => {
        const eventColumns = eventList.reduce(
          (eventResult, { name, type }) => ({
            ...eventResult,
            [name]: eventResult[name] ? `${eventResult[name]}, ${type}` : type,
          }),
          {}
        );
        return [...result, { date: key, ...eventColumns }];
      },
      []
    );

    const eventMonthsMap = months.reduce((result, current) => {
      const eventFilter = Object.entries(eventDaysMap).reduce(
        (eventResult, [date, eventList]) => {
          return moment(current).isSame(moment(date), "month")
            ? [...eventResult, ...eventList]
            : eventResult;
        },
        []
      );
      return eventFilter.length
        ? {
            ...result,
            [current]: result[current]
              ? [...result[current], eventFilter]
              : eventFilter,
          }
        : result;
    }, {});
    const monthsMap = Object.entries(eventMonthsMap).reduce(
      (result, [key, eventList]) => {
        const eventColumns = eventList.reduce(
          (eventResult, { name, type }) => ({
            ...eventResult,
            [name]: eventResult[name]
              ? eventResult[name] + getCount(type)
              : getCount(type),
          }),
          {}
        );
        return [...result, { date: key, ...eventColumns }];
      },
      []
    );

    const eventYearsMap = years.reduce((result, current) => {
      const eventFilter = Object.entries(eventDaysMap).reduce(
        (eventResult, [date, eventList]) => {
          return moment(current).isSame(moment(date), "year")
            ? [...eventResult, ...eventList]
            : eventResult;
        },
        []
      );
      return eventFilter.length
        ? {
            ...result,
            [current]: result[current]
              ? [...result[current], eventFilter]
              : eventFilter,
          }
        : result;
    }, {});
    const yearsMap = Object.entries(eventYearsMap).reduce(
      (result, [key, eventList]) => {
        const eventColumns = eventList.reduce(
          (eventResult, { name, type }) => ({
            ...eventResult,
            [name]: eventResult[name]
              ? eventResult[name] + getCount(type)
              : getCount(type),
          }),
          {}
        );
        return [...result, { date: key, ...eventColumns }];
      },
      []
    );

    res.status(200).json({ names, daysMap, monthsMap, yearsMap });
  } catch (e) {
    console.error(e);
    res.status(500).json({ error: e });
  }
};
