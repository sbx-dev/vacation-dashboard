import React from "react";
import App from "next/app";
import Layout from "../component/layout";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";

const MyApp = ({ Component, pageProps }) => (
  <Layout {...pageProps}>
    <Component {...pageProps} />
  </Layout>
);

MyApp.getInitialProps = async (appContext) => {
  const { pageProps = {} } = await App.getInitialProps(appContext);
  const profile = appContext.ctx?.req?.session?.passport?.user;
  return {
    pageProps: {
      profile,
      ...pageProps,
    },
  };
};

export default MyApp;
