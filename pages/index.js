import React, { useEffect, useState } from "react";
import axios from "axios";
import moment from "moment";
import Table from "../component/table";
import exportExcel from "../lib/excel";
import { Button, Col, FormControl, InputGroup, Row } from "react-bootstrap";

const Index = () => {
  const [timeMin, setTimeMin] = useState(
    moment().startOf("year").format("YYYY")
  );
  const [timeMax, setTimeMax] = useState(moment().endOf("year").format("YYYY"));
  const [data, setData] = useState({});

  const getData = async (callback) => {
    const { data } = await axios({
      url: "/api/vacation",
      params: {
        timeMin,
        timeMax,
      },
    });
    callback && callback(data);
  };
  const getColumn = () => {
    const { names } = data;
    return names
      ? names.reduce(
          (result, current) => [
            ...result,
            { dataField: current, text: current },
          ],
          [
            {
              dataField: "date",
              text: "날짜",
              sort: true,
              headerStyle: { width: 100 },
            },
          ]
        )
      : [{ dataField: "Loading", text: "Loading" }];
  };
  const getSorded = () => [
    {
      dataField: "date",
      order: "desc",
    },
  ];
  const submit = () => {
    setData({});
    getData(setData);
  };

  const excelExport = () => {
    exportExcel(
      {
        year_vacation: data.yearsMap,
        month_vacation: data.monthsMap,
        day_vacation: data.daysMap,
      },
      "vacation"
    );
  };

  useEffect(() => {
    getData(setData);
  }, []);

  return (
    <>
      <Row className={`justify-content-between`}>
        <Col md={2}>
          <Button block onClick={excelExport}>
            excel download
          </Button>
        </Col>
        <Col md={5}>
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text>Select Year</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder={`StartYear`}
              defaultValue={timeMin}
              onChange={(e) => {
                setTimeMin(e.target.value);
              }}
            />
            <InputGroup.Prepend>
              <InputGroup.Text>~</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder={`EndYear`}
              defaultValue={timeMax}
              onChange={(e) => {
                setTimeMax(e.target.value);
              }}
            />
            <InputGroup.Append>
              <Button onClick={submit}>Select</Button>
            </InputGroup.Append>
          </InputGroup>
        </Col>
      </Row>
      <Table
        title={`Years`}
        keyField="date"
        columns={getColumn()}
        data={data.yearsMap}
        defaultSorted={getSorded()}
      />
      <Table
        title={`Months`}
        keyField="date"
        columns={getColumn()}
        data={data.monthsMap}
        defaultSorted={getSorded()}
      />
      <Table
        title={`Days`}
        keyField="date"
        columns={getColumn()}
        data={data.daysMap}
        defaultSorted={getSorded()}
        pagenation
      />
    </>
  );
};

export default Index;
