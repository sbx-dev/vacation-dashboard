const session = require('express-session');

const sessionOption = {
  secret : 'socialbxAdmin',
  cookie: { maxAge: 60 * 60 * 1000 },
  resave: false,
  saveUninitialized: true
};

module.exports = session(sessionOption);