import XLSX from 'xlsx';
import { saveAs } from 'file-saver';

const createWorkbook = () => ( XLSX.utils.book_new() );
const createSheet = (data) => (XLSX.utils.json_to_sheet(data));
const addSheet = (workbook, worksheet, sheetName) => {XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)};

//공통
// 참고 출처 : https://redstapler.co/sheetjs-tutorial-create-xlsx/
const s2ab = (s) => {
  const buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
  const view = new Uint8Array(buf);  //create uint8array as viewer
  for (let i=0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
  return buf;
};

const exportExcel = (dataObj = {}, fileName = 'empty') => {
  // step 1. workbook 생성
  const workbook = createWorkbook();

  Object.entries(dataObj).forEach(([name, data]) => {
    // step 2. 시트 만들기
    const worksheet = createSheet(data);

    // step 3. workbook에 새로만든 워크시트에 이름을 주고 붙인다.
    addSheet(workbook, worksheet, name);
  });

  // step 4. 엑셀 파일 만들기
  const excel = XLSX.write(workbook, { bookType:'xlsx',  type: 'binary' });

  // step 5. 엑셀 파일 내보내기
  saveAs(new Blob([s2ab(excel)],{type:"application/octet-stream"}), `${fileName}.xlsx`);
};

export default exportExcel;