require("dotenv").config();
const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const axios = require("axios");

const passportOption = {
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET_ID,
  callbackURL: process.env.GOOGLE_CALLBACK_URL,
};
const passportCallback = async (accessToken, refreshToken, profile, done) => {
  const hrGroupId = process.env.GOOGLE_HR_GROUP_ID;
  const key = process.env.GOOGLE_APP_KEY;
  let hrTeam;

  try {
    const { data } = await axios({
      url: `https://www.googleapis.com/admin/directory/v1/groups/${hrGroupId}/members/${profile.id}`,
      // url: `https://www.googleapis.com/admin/directory/v1/groups/${hrGroupId}/members/102328484381463704568`,
      headers: { Authorization: `Bearer ${accessToken}` },
      params: {
        key,
      },
    });

    hrTeam = data;
  } catch (e) {
    // console.error('error', e);
  }

  done(null, { accessToken, refreshToken, ...profile, hrTeam });
};
const google = new GoogleStrategy(passportOption, passportCallback);

passport.use(google);
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));

module.exports = passport;
