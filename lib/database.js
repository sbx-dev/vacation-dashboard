const mysql = require("mysql2/promise");

const databaseOptions = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
};

let pool;
const getPool = () => {
  pool = pool || mysql.createPool(databaseOptions);
  return pool;
};

const query = async (query, params) => {
  const connection = await getPool().getConnection();
  try {
    return await connection.query(query, params);
  } catch (e) {
    console.log(e);
    return undefined;
  } finally {
    connection.release();
  }
};

module.exports = {
  getPool,
  query,
};
