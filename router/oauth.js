const express = require('express');
const passport = require('passport');
const router = express.Router();

const loginOption = {
  hd: 'socialbox.im',
  scope: [
    'profile',
    'email',
    'openid',
    'https://www.googleapis.com/auth/admin.directory.group.member.readonly',
    'https://www.googleapis.com/auth/calendar.events.readonly'
  ]};
const callbackOption = {successRedirect : '/', failureRedirect: '/'};
router.get('/oauth/login', passport.authenticate('google', loginOption));
router.get('/oauth/callback', passport.authenticate('google', callbackOption));
router.get('/oauth/logout', (req, res) => {req.logout(); return res.redirect('/')});

module.exports = router;
